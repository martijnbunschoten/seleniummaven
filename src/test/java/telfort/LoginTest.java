package telfort;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginTest {

    @Test
    public void startWebDriver() {

        ChromeDriverManager.getInstance().setup();

        WebDriver driver = new ChromeDriver();

        driver.navigate().to("https://www.telfort.nl/persoonlijk/inloggen-mijn-telfort.htm#/login");
        Assert.assertTrue("title should start with Inloggen Mijn ",
                driver.getTitle().startsWith("Inloggen Mijn"));

        driver.close();

        driver.quit();
    }

}



